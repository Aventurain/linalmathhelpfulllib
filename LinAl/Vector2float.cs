﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinAl
{
    public struct Vector2float
    {
        public float X, Y;

        public float Modulus
        {
            get
            {
                if (X == 0 && Y == 0)
                    return 0;
                return (float)(Math.Sqrt(X * X + (Y * Y)));
            }
        }

        public float Lenght
        {
            get
            {
                return Modulus; //Библиотека должна поддерживать разные состояния разумности её владельца!
            }
        }

        public float Angle//между вектором и вектором (1 , 0)  
        {
            get
            {
                float angle = (float)Math.Acos((X / (Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2)))));
                if (Y < 0)
                    return (float)((2 * Math.PI - angle) * 180 / Math.PI);
                return (float)(angle * 180 / Math.PI);
            }
        }

        //public Vector2float() {}

        public Vector2float(float x, float y)
        {
            X = x;
            Y = y;
        }

        public void RotateByRad(float angle)
        {
            float tempX = (float)(X * Math.Cos(angle) - Y * Math.Sin(angle));
            Y = (float)(X * Math.Sin(angle) + Y * Math.Cos(angle));
            X = tempX;
        }

        public void RotateByDegree(float angle)
        {
            angle = angle * (float)Math.PI / 180;

            float tempX = (float)(X * Math.Cos(angle) - Y * Math.Sin(angle));
            Y = (float)(X * Math.Sin(angle) + Y * Math.Cos(angle));
            X = tempX;
        }

        public void Normalize()
        {
            float lenght = Lenght;
            if (lenght == 0) return;
            X /= lenght;
            Y /= lenght;
        }

        public float GetAngleWith(Vector2float vector)
        {
            float lenght1 = Lenght;
            float lenght2 = vector.Lenght;
            if (lenght1 == 0 || lenght2 == 0) return 0;
            //float a3 = ((vector1.X * vector2.X) + (vector1.Y * vector2.Y)) / (GetLenght(vector1) * GetLenght(vector2));
            float angle = (float)Math.Acos(Math.Round(((X * vector.X) + (Y * vector.Y)) / (lenght1 * lenght2), 4));
            return (float)(angle * 180 / Math.PI);
        }

        public static float operator *(Vector2float vector1, Vector2float vector2)
        {
            return vector1.X * vector2.X + (vector1.Y * vector2.Y);
        }

        public static Vector2float operator +(Vector2float vector, Vector2float vector2)
        {
            vector.X += vector2.X;
            vector.Y += vector2.Y;
            return vector;
        }

        public static Vector2float operator -(Vector2float vector, Vector2float vector2)
        {
            vector.X -= vector2.X;
            vector.Y -= vector2.Y;
            return vector;
        }

        public static Vector2float operator *(Vector2float vector, float num)
        {
            vector.X *= num;
            vector.Y *= num;
            return vector;
        }

        public static Vector2float operator /(Vector2float vector, float num)
        {
            vector.X /= num;
            vector.Y /= num;
            return vector;
        }

        public static Vector2float operator *(float num, Vector2float vector)
        {
            return vector * num;
        }

        public static Vector2float operator /(float num, Vector2float vector)
        {
            return vector / num;
        }

        public static bool operator !=(Vector2float v1, Vector2float v2)
        {
            if (v1.X != v2.X || v1.Y != v2.Y) return true;
            return false;
        }

        public static bool operator ==(Vector2float v1, Vector2float v2)
        {
            if (v1.X == v2.X && v1.Y == v2.Y) return true;
            return false;
        }
    }
}
