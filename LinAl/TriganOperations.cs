﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinAl
{
    public class TriganOperations
    {
        public static float getDegreFromCos(float num)
        {
            return (float)(Math.Acos(num) * 180 / Math.PI);
        }
    }
}
