﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinAl
{
    public class Line
    {
        public Vector2float startPos;
        public Vector2float endPos;

        public Line(Vector2float start, Vector2float end)
        {
            startPos = start;
            endPos = end;
        }

        public bool IsCrossingWithLine(Line line2)
        {
            if (startPos == line2.startPos || startPos == line2.endPos)
                return true;
            if (endPos == line2.startPos || endPos == line2.endPos)
                return true;
            return Orientation(startPos, endPos, line2.startPos) != Orientation(startPos, endPos, line2.endPos) &&
                Orientation(line2.startPos, line2.endPos, endPos) != Orientation(line2.startPos, line2.endPos, startPos);
        }

        private bool Orientation(Vector2float p1, Vector2float p2, Vector2float p3)
        {
            double slope = (p2.Y - p1.Y) * (p3.X - p2.X) - (p3.Y - p2.Y) * (p2.X - p1.X);
            //clockwise is screen space (angle increases) - false
            return slope < -0.00001;
        }

        public Vector2float GetPointAt(float percent)
        {
            return new Vector2float((endPos.X - startPos.X) * percent + startPos.X,
                (endPos.Y - startPos.Y) * percent + startPos.Y);
        }

        public double Lenght
        {
            get
            {
                return Math.Sqrt(Math.Pow(endPos.X - startPos.X, 2) + Math.Pow(endPos.Y - startPos.Y, 2));
            }
        }

        public Vector2float DirectionVector
        {
            get
            {
                return new Vector2float(endPos.X - startPos.X, endPos.Y - startPos.Y);
            }
        }

        public float Direction
        {
            get
            {
                return DirectionVector.Angle;
            }
        }
    }
}
